/*
    Combined compilation of pwnlink
 */

#include "esp.h"

/*
    Source from /tmp/app_go/appweb/pwnlink/dist/public/login.esp
 */
/*
   Generated from dist/public/login.esp
 */
#include "esp.h"

static void view_de9147cd7265ac2c53d13e3f80697cdb(HttpConn *conn) {
  espRenderBlock(conn, "<!DOCTYPE html>\n\
<html lang=\"en\">\n\
<head>\n\
    <title>Login — PWN-Link</title>\n\
<meta charset=\"utf-8\" />\n\
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\
<link href=\"../lib/bootstrap/css/bootstrap-grid.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"../lib/bootstrap/css/bootstrap-reboot.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"../lib/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"../css/default.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    \n\
\n\
</head>\n\
<body>\n\
    <div class=\"container text-center\">\n\
\n\
\n\
<form class=\"form-login\" method=\"POST\" action=\"/auth/login\">\n\
    <img class=\"mb-3\" src=\"../media/logo.svg\" width=\"72\" height=\"72\">\n\
    <h1 class=\"h3 mb-3\">PWN-Link</h1>\n\
\n\
    <label for=\"username\" class=\"sr-only\">Username</label>\n\
    <input type=\"text\" class=\"form-control\" id=\"username\" name=\"username\" placeholder=\"Username\" required autofocus>\n\
\n\
    <label for=\"password\" class=\"sr-only\">Password</label>\n\
    <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" placeholder=\"Password\" required>\n\
\n\
    <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Login</button>\n\
\n\
    ", 1214);
inputSecurityToken();   espRenderBlock(conn, "\n\
</form>\n\
\n\
\n\
        <footer class=\"container\">\n\
    <div class=\"row\">\n\
        <div class=\"col-12 col-md\">\n\
            <small class=\"d-block mb-3 text-muted\">&copy; 2048, Practically <b>W</b>ulnerable Networks</small>\n\
        </div>\n\
    </div>\n\
</footer>\n\
\n\
<script src=\"../lib/bootstrap/js/bootstrap.bundle.min.js\"></script>\n\
    <script src=\"../lib/bootstrap/js/bootstrap.min.js\"></script>\n\
    \n\
\n\
    </div>\n\
</body>\n\
</html>\n\
", 418);
}

ESP_EXPORT int esp_view_de9147cd7265ac2c53d13e3f80697cdb(HttpRoute *route, MprModule *module) {
   espDefineView(route, "public/login.esp", view_de9147cd7265ac2c53d13e3f80697cdb);
   return 0;
}


/*
    Source from /tmp/app_go/appweb/pwnlink/dist/expert-settings.esp
 */
/*
   Generated from dist/expert-settings.esp
 */
#include "esp.h"

static void view_3b382a21ab0b3d85868f1dac828df0c1(HttpConn *conn) {
  espRenderBlock(conn, "<!DOCTYPE html>\n\
<html lang=\"en\">\n\
<head>\n\
    <title>Expert settings — PWN-Link</title>\n\
<meta charset=\"utf-8\" />\n\
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\
<link href=\"lib/bootstrap/css/bootstrap-grid.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"lib/bootstrap/css/bootstrap-reboot.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"lib/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"css/default.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    \n\
\n\
</head>\n\
<body>\n\
    <div class=\"container\">\n\
        <div class=\"container\">\n\
    <nav class=\"navbar navbar-expand navbar-light bg-light\">\n\
        <a class=\"navbar-brand\" href=\"./\">\n\
            <img src=\"./media/logo.svg\" width=\"48\" height=\"48\">\n\
            PWN-Link\n\
        </a>\n\
\n\
        <ul class=\"navbar-nav mr-auto\">\n\
            <li class=\"nav-item\">\n\
                <a class=\"nav-link\" href=\"./\">Quick setup</a>\n\
            </li>\n\
            <li class=\"nav-item\">\n\
                <a class=\"nav-link\" href=\"./expert-settings.esp\">Expert settings</a>\n\
            </li>\n\
            <li class=\"nav-item\">\n\
                <a class=\"nav-link\" href=\"./maintenance.esp\">Maintenance</a>\n\
            </li>\n\
        </ul>\n\
        <a href=\"./auth/logout\">Logout</a>\n\
    </nav>\n\
</div>\n\
\n\
\n\
\n\
\n\
<div class=\"card m-3 p-3\">\n\
    <h2 class=\"h3\">Wi-Fi tuning</h2>\n\
    <form>\n\
        <div class=\"form-group\">\n\
            <label for=\"dbm\">TX power</label>\n\
            <div class=\"input-group\">\n\
                <input type=\"text\" class=\"form-control\" id=\"dbm\" name=\"dbm\" value=\"23\">\n\
                <div class=\"input-group-append\">\n\
                    <div class=\"input-group-text\">dBm</div>\n\
                </div>\n\
            </div>\n\
            <small class=\"form-text text-muted\">The value goes directly to the power amplifier chip.</small>\n\
        </div>\n\
        <button type=\"submit\" class=\"btn btn-primary\">Save</button>\n\
    </form>\n\
</div>\n\
\n\
\n\
        <footer class=\"container\">\n\
    <div class=\"row\">\n\
        <div class=\"col-12 col-md\">\n\
            <small class=\"d-block mb-3 text-muted\">&copy; 2048, Practically <b>W</b>ulnerable Networks</small>\n\
        </div>\n\
    </div>\n\
</footer>\n\
\n\
<script src=\"lib/bootstrap/js/bootstrap.bundle.min.js\"></script>\n\
    <script src=\"lib/bootstrap/js/bootstrap.min.js\"></script>\n\
    \n\
\n\
    </div>\n\
</body>\n\
</html>\n\
", 2372);
}

ESP_EXPORT int esp_view_3b382a21ab0b3d85868f1dac828df0c1(HttpRoute *route, MprModule *module) {
   espDefineView(route, "expert-settings.esp", view_3b382a21ab0b3d85868f1dac828df0c1);
   return 0;
}


/*
    Source from controllers/do.c
 */
#include "esp.h"

static void
ping(HttpConn *conn)
{
	char *command;
	command = sfmt("ping -c 4 %s", param("ip"));
	system(command);
	espRedirectBack(conn);
  

}

static void
do_save(HttpConn *conn)
{
    
    //param("UpnP");
    //param("_traff_en");
    //param("mult_stream");
    //param("wirel");

    char UpnP[15];
    char _traff_en[15];
    char mult_stream[15];
    char wirel[15];
    char wan_[15];

    char *c = param("UpnP");
    if (c != NULL) {
        if (strlen(c) > 0) {
            strncpy(UpnP, param("UpnP"), 15);
        }
    }

    char *cc = param("_traff_en");
    if (cc != NULL)
        if (strlen(param("_traff_en")) > 0)
            strncpy(_traff_en, param("_traff_en"), 15);

    char *ccc = param("mult_stream");
    if (ccc != NULL)
        if (strlen(param("mult_stream")) > 0)
            strncpy(mult_stream, param("mult_stream"), 15);

    char *cccc = param("wirel");
    if (cccc != NULL)
        if (strlen(param("wirel")) > 0)
            strncpy(wirel, param("wirel"), 15);

    // # stack overflow
    char *ccccc = param("wan_");
        if (ccccc != NULL)
        if (strlen(param("wan_")) > 0)
            strcpy(wan_, param("wan_"));

	espRedirectBack(conn);
}

static void
do_speed(HttpConn *conn)
{
    
    
    struct profile {
        int speed_limit_output;
        int speed_limit_input;
        char *name;
    };
     
   
    struct profile *prof1, *prof2;

    prof1 = malloc(sizeof(struct profile));
    prof1->speed_limit_output = (int) param("speed_limit_output1");
    prof1->speed_limit_input = (int) param("speed_limit_input1");
    prof1->name = malloc(40);

    prof2 = malloc(sizeof(struct profile));
    prof2->speed_limit_output = (int) param("speed_limit_output2");
    prof2->speed_limit_input = (int) param("speed_limit_input2");
    prof2->name = malloc(40);

    
    // # heap overflow
    strcpy(prof1->name, param("name1"));
    strcpy(prof2->name, param("name2"));
    
    
    espRedirectBack(conn);
}


static void
do_privel_auth(HttpConn *conn)
{
	char *login, *password, *secret_par;

    login = malloc(32);
    password = malloc(32);
    secret_par = malloc(32);
	char test[50];

    strcpy(login, param("login"));
    strcpy(password, param("password"));
    strcpy(secret_par, param("secret"));

	if (strcmp(login, "Boris") != 0)
		if (strcmp(secret_par, "superstrong") != 0)
			strcpy(test, password);
		

    free(secret_par);
    free(password);
    free(login);


	espRedirectBack(conn);
  

}

static void
super_simple(HttpConn *conn)
{
    int max = atoi(param("p"));

    if (max > 1000)
        render("error!");
    
    char * ptr = (char *) malloc(sizeof(char) * max);
    ptr[0] = 'a';
    free(ptr);

	espRedirectBack(conn);
}

static void
cookies(HttpConn *conn)
{
    char buf[0x100];
    char * s = httpGetCookies(conn);
    strcpy(buf, s);

	espRedirectBack(conn);
}

ESP_EXPORT int
esp_controller_pwnlink_do(HttpRoute *route, MprModule *module)
{
	espDefineAction(route, "do/ping", ping);
	espDefineAction(route, "do/do_save", do_save);
	espDefineAction(route, "do/do_speed", do_speed);
	espDefineAction(route, "do/do_privel_auth", do_privel_auth);
	espDefineAction(route, "do/super_simple", super_simple);
    espDefineAction(route, "do/cookies", cookies);

	return 0;
}


/*
    Source from /tmp/app_go/appweb/pwnlink/dist/maintenance.esp
 */
/*
   Generated from dist/maintenance.esp
 */
#include "esp.h"

static void view_9f8d943a6fb8d1b66a3e4b95cc39c999(HttpConn *conn) {
  espRenderBlock(conn, "<!DOCTYPE html>\n\
<html lang=\"en\">\n\
<head>\n\
    <title>Maintenance — PWN-Link</title>\n\
<meta charset=\"utf-8\" />\n\
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\
<link href=\"lib/bootstrap/css/bootstrap-grid.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"lib/bootstrap/css/bootstrap-reboot.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"lib/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"css/default.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    \n\
\n\
</head>\n\
<body>\n\
    <div class=\"container\">\n\
        <div class=\"container\">\n\
    <nav class=\"navbar navbar-expand navbar-light bg-light\">\n\
        <a class=\"navbar-brand\" href=\"./\">\n\
            <img src=\"./media/logo.svg\" width=\"48\" height=\"48\">\n\
            PWN-Link\n\
        </a>\n\
\n\
        <ul class=\"navbar-nav mr-auto\">\n\
            <li class=\"nav-item\">\n\
                <a class=\"nav-link\" href=\"./\">Quick setup</a>\n\
            </li>\n\
            <li class=\"nav-item\">\n\
                <a class=\"nav-link\" href=\"./expert-settings.esp\">Expert settings</a>\n\
            </li>\n\
            <li class=\"nav-item\">\n\
                <a class=\"nav-link\" href=\"./maintenance.esp\">Maintenance</a>\n\
            </li>\n\
        </ul>\n\
        <a href=\"./auth/logout\">Logout</a>\n\
    </nav>\n\
</div>\n\
\n\
\n\
\n\
\n\
<div class=\"card m-3 p-3\">\n\
    <h2 class=\"h3\">Host reachability</h2>\n\
    <form action=\"./do/ping\">\n\
        <div class=\"form-group\">\n\
            <label for=\"ip\">IP-address</label>\n\
            <input type=\"text\" class=\"form-control\" id=\"ip\" name=\"ip\" value=\"8.8.8.8\">\n\
            <small class=\"form-text text-muted\">Send ICMP packets to this address.</small>\n\
        </div>\n\
        <button type=\"submit\" class=\"btn btn-primary\">Ping</button>\n\
    </form>\n\
</div>\n\
\n\
<div class=\"card m-3 p-3\">\n\
    <h2 class=\"h3\">Update settings</h2>\n\
    <form action=\"./do/do_save\"  method=\"POST\">\n\
        <!-- <div class=\"form-group\">\n\
            <label for=\"ip\">settings:</label>\n\
            <small class=\"form-text text-muted\">Change and send changes.</small>\n\
        </div> -->\n\
            <p>Advanced Wireless</p>\n\
            <p><input type=\"checkbox\" name=\"UpnP\" value=\"UpnP\">Enable UpnP</p>\n\
            <p><input type=\"checkbox\" name=\"wan_\" value=\"wan_\" checked>Enable WAN Ping Response</p>\n\
            <p><input type=\"checkbox\" name=\"_traff_en\" value=\"_traff_en\" checked>Enable Traffic Management</p>\n\
            <p><input type=\"checkbox\" name=\"mult_stream\" value=\"mult_stream\">Enable Multicast Streams</p>\n\
            <p><input type=\"checkbox\" name=\"wirel\" value=\"wirel\">Wireless Enhance Mode</p>\n\
        <button type=\"submit\" class=\"btn btn-primary\">Save settings</button>\n\
        <button type=\"submit\" class=\"btn btn-primary\">Don't Save Settings</button>\n\
    </form>\n\
</div>\n\
\n\
<div class=\"card m-3 p-3\">\n\
    <h2 class=\"h3\">Speed profile</h2>\n\
    <form action=\"./do/do_speed\">\n\
        <!-- <div class=\"form-group\">\n\
            <label for=\"ip\">settings:</label>\n\
            <small class=\"form-text text-muted\">Change and send changes.</small>\n\
        </div> -->\n\
            <p>Management</p>\n\
\n\
            <!--<p><input type=\"text\" name=\"name_len\" value=\"name_len\">name len</p>-->\n\
\n\
            <p>Profile 1:</p>\n\
            <p><input type=\"text\" name=\"name1\" value=\"name1\">name of prof1</p>\n\
            <p><input type=\"text\" name=\"speed_limit_input1\" value=\"100\"> input speed of prof1</p>\n\
            <p><input type=\"text\" name=\"speed_limit_output1\" value=\"100\"> ouput speed of prof1</p>\n\
\n\
            <p>Profile 2:</p>\n\
            <p><input type=\"text\" name=\"name2\" value=\"name2\">name of prof1</p>\n\
            <p><input type=\"text\" name=\"speed_limit_input2\" value=\"200\"> input speed of prof1</p>\n\
            <p><input type=\"text\" name=\"speed_limit_output2\" value=\"200\"> ouput speed of prof1</p>\n\
            \n\
\n\
        <button type=\"submit\" class=\"btn btn-primary\">send settings</button>\n\
    </form>\n\
</div>\n\
\n\
<div class=\"card m-3 p-3\">\n\
<form method=\"POST\" action=\"/do/do_privel_auth\">\n\
    <p>privileged users</p>\n\
\n\
    <label for=\"login\" class=\"sr-only\">login</label>\n\
    <p><input type=\"text\" id=\"login\" name=\"login\" required autofocus></p>\n\
\n\
    <p><label for=\"password\" class=\"sr-only\">Password</label></p>\n\
    <p><input type=\"password\" id=\"password\" name=\"password\"></p>\n\
    <p><input type=\"text\" id=\"secret_par\" name=\"secret_par\"></p>\n\
\n\
    <button class=\"btn-primary\" type=\"submit\">Login</button>\n\
\n\
    \n\
</form>\n\
</div>\n\
\n\
<div class=\"card m-3 p-3\">\n\
    <h2 class=\"h3\">max input speed</h2>\n\
    <form action=\"./do/super_simple\">\n\
        \n\
            <p>super simple overflow</p>\n\
\n\
            <p><input type=\"text\" name=\"p\"> mbit/s </p>\n\
\n\
        <button type=\"submit\" class=\"btn btn-primary\">save settings</button>\n\
    </form>\n\
</div>\n\
\n\
\n\
<div class=\"card m-3 p-3\">\n\
    <h2 class=\"h3\">somethings input</h2>\n\
    <form action=\"./do/cookies\">\n\
        \n\
            <p>cookies overflow</p>\n\
\n\
            <p><input type=\"text\" name=\"c\"> somethings </p>\n\
\n\
        <button type=\"submit\" class=\"btn btn-primary\">save settings</button>\n\
    </form>\n\
</div>\n\
\n\
\n\
<div class=\"card m-3 p-3\">\n\
    <h2 class=\"h3\">Power controls</h2>\n\
    <form>\n\
        <button type=\"submit\" class=\"btn btn-danger\">Reboot</button>\n\
    </form>\n\
</div>\n\
\n\
\n\
        <footer class=\"container\">\n\
    <div class=\"row\">\n\
        <div class=\"col-12 col-md\">\n\
            <small class=\"d-block mb-3 text-muted\">&copy; 2048, Practically <b>W</b>ulnerable Networks</small>\n\
        </div>\n\
    </div>\n\
</footer>\n\
\n\
<script src=\"lib/bootstrap/js/bootstrap.bundle.min.js\"></script>\n\
    <script src=\"lib/bootstrap/js/bootstrap.min.js\"></script>\n\
    \n\
\n\
    </div>\n\
</body>\n\
</html>\n\
", 5610);
}

ESP_EXPORT int esp_view_9f8d943a6fb8d1b66a3e4b95cc39c999(HttpRoute *route, MprModule *module) {
   espDefineView(route, "maintenance.esp", view_9f8d943a6fb8d1b66a3e4b95cc39c999);
   return 0;
}


/*
    Source from controllers/auth.c
 */
#include "esp.h"

static void
login(HttpConn *conn)
{
	if (httpLogin(conn, param("username"), param("password"))) {
		redirect("/");
    }
	else {
        //  #1 stack buffer overflow
        char b[10];
        strcpy(b, param("username"));

        //  #2 stack buffer overflow in cookie header
        char buf[0x100];
        strcpy(buf, httpGetCookies(conn));

		redirect("/public/login.esp");
    }
}

static void
logout(HttpConn *conn)
{
	httpLogout(conn);
	redirect("/public/login.esp");
}

ESP_EXPORT int
esp_controller_pwnlink_auth(HttpRoute *route, MprModule *module)
{
	espDefineAction(route, "auth/login", login);
	espDefineAction(route, "auth/logout", logout);

	return 0;
}


/*
    Source from /tmp/app_go/appweb/pwnlink/dist/index.esp
 */
/*
   Generated from dist/index.esp
 */
#include "esp.h"

static void view_5049a6bfd2f769a4caaf9018a56abaf2(HttpConn *conn) {
  espRenderBlock(conn, "<!DOCTYPE html>\n\
<html lang=\"en\">\n\
<head>\n\
    <title>Quick setup — PWN-Link</title>\n\
<meta charset=\"utf-8\" />\n\
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\
<link href=\"lib/bootstrap/css/bootstrap-grid.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"lib/bootstrap/css/bootstrap-reboot.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"lib/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    <link href=\"css/default.css\" rel=\"stylesheet\" type=\"text/css\" />\n\
    \n\
\n\
</head>\n\
<body>\n\
    <div class=\"container\">\n\
        <div class=\"container\">\n\
    <nav class=\"navbar navbar-expand navbar-light bg-light\">\n\
        <a class=\"navbar-brand\" href=\"./\">\n\
            <img src=\"./media/logo.svg\" width=\"48\" height=\"48\">\n\
            PWN-Link\n\
        </a>\n\
\n\
        <ul class=\"navbar-nav mr-auto\">\n\
            <li class=\"nav-item\">\n\
                <a class=\"nav-link\" href=\"./\">Quick setup</a>\n\
            </li>\n\
            <li class=\"nav-item\">\n\
                <a class=\"nav-link\" href=\"./expert-settings.esp\">Expert settings</a>\n\
            </li>\n\
            <li class=\"nav-item\">\n\
                <a class=\"nav-link\" href=\"./maintenance.esp\">Maintenance</a>\n\
            </li>\n\
        </ul>\n\
        <a href=\"./auth/logout\">Logout</a>\n\
    </nav>\n\
</div>\n\
\n\
\n\
\n\
\n\
<div class=\"card m-3 p-3\">\n\
    <h2 class=\"h3\">LAN</h2>\n\
    <form>\n\
        <div class=\"form-group\">\n\
            <label for=\"ip\">IP-address</label>\n\
            <input type=\"text\" class=\"form-control\" id=\"ip\" name=\"ip\" value=\"192.168.0.1\">\n\
            <small class=\"form-text text-muted\">Your router will be accessible through this IP.</small>\n\
        </div>\n\
        <div class=\"form-group\">\n\
            <label for=\"netmask\">Netmask</label>\n\
            <input type=\"text\" class=\"form-control\" id=\"netmask\" name=\"netmask\" value=\"255.255.255.0\">\n\
            <small class=\"form-text text-muted\">Your router will use this broadcast address.</small>\n\
        </div>\n\
        <button type=\"submit\" class=\"btn btn-primary\">Save</button>\n\
    </form>\n\
</div>\n\
\n\
\n\
        <footer class=\"container\">\n\
    <div class=\"row\">\n\
        <div class=\"col-12 col-md\">\n\
            <small class=\"d-block mb-3 text-muted\">&copy; 2048, Practically <b>W</b>ulnerable Networks</small>\n\
        </div>\n\
    </div>\n\
</footer>\n\
\n\
<script src=\"lib/bootstrap/js/bootstrap.bundle.min.js\"></script>\n\
    <script src=\"lib/bootstrap/js/bootstrap.min.js\"></script>\n\
    \n\
\n\
    </div>\n\
</body>\n\
</html>\n\
", 2470);
}

ESP_EXPORT int esp_view_5049a6bfd2f769a4caaf9018a56abaf2(HttpRoute *route, MprModule *module) {
   espDefineView(route, "index.esp", view_5049a6bfd2f769a4caaf9018a56abaf2);
   return 0;
}



ESP_EXPORT int esp_app_pwnlink_combine(HttpRoute *route, MprModule *module) {
    esp_view_de9147cd7265ac2c53d13e3f80697cdb(route, module);
    esp_view_3b382a21ab0b3d85868f1dac828df0c1(route, module);
    esp_controller_pwnlink_do(route, module);
    esp_view_9f8d943a6fb8d1b66a3e4b95cc39c999(route, module);
    esp_controller_pwnlink_auth(route, module);
    esp_view_5049a6bfd2f769a4caaf9018a56abaf2(route, module);
    return 0;
}
